# Knockknock

A CLI to check the status of https://gitlab.com or https://about.gitlab.com and report an average response time after probing the site every 10 seconds for a one minute.

## Usage

- Clone repository
- Run with `bundle exec exe/knockknock ping`

## Development

- Developed with Ruby 2.4.1

## Testing

- Run `bundle exec rspec`

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/martin308/knockknock. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Knockknock project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/martin308/knockknock/blob/master/CODE_OF_CONDUCT.md).
