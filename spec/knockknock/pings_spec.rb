require "net/ping/tcp"

RSpec.describe Knockknock::Pings do
  describe "ping" do
    let(:ping_type_instance) do
      double("ping_type_instance").tap do |instance|
        allow(instance).to receive(:ping)
        allow(instance).to receive(:duration) { 1 }
      end
    end

    let(:ping_type) do
      double("ping_type").tap do |ping_type|
        allow(ping_type).to receive(:new) { ping_type_instance }
      end
    end

    let(:host) { "gitlab.com" }

    subject(:pings) do
      Knockknock::Pings.new(host: host, ping_type: ping_type)
    end

    it "can ping" do
      ping = pings.ping
      expect(ping.duration).to be 1
    end

    it "enumerates the pings" do
      allow(pings).to receive(:ping) { 1 }
      expect(pings.take(6)).to eq [1, 1, 1, 1, 1, 1]
    end

    it "allows an interval to be specified" do
      allow(pings).to receive(:ping) { 1 }
      expect(pings.with_interval(1).take(6)).to eq [1, 1, 1, 1, 1, 1]
    end

    it "generates the correct stats" do
      stats = pings.generate_stats(interval: 1, times: 6)
      expect(stats).to have_attributes(count: 6, average: 1000.0, host: host)
    end

    it "handles a bung host" do
      pings = Knockknock::Pings.new(host: "not_a_web_site", ping_type: ping_type)
      pings.ping
    end
  end
end
