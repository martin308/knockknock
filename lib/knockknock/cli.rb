require "thor"
require "knockknock"

module Knockknock
  DEFAULT_HOSTS = %w[https://gitlab.com https://about.gitlab.com].freeze

  # Defines the CLI commands that we respond to. The main entry to the CLI app
  class CLI < Thor
    desc "version", "knockknock version"
    def version
      puts Knockknock::VERSION
    end

    map "-v" => "version"

    desc "ping", "pings the sites"
    option :hosts, type: :array, default: DEFAULT_HOSTS
    option :times, type: :numeric, default: 6
    option :interval, type: :numeric, default: 10
    def ping
      results = {}
      threads = options[:hosts].map do |host|
        puts "pinging #{host} #{options[:times]} times over #{options[:times] * options[:interval]} seconds"
        Thread.new do
          results[host] = Pings.new(host: host)
                               .generate_stats(interval: options[:interval], times: options[:times])
        end
      end
      threads.each(&:join)
      results.each do |_key, stats|
        puts "#{stats.host} over #{stats.count} requests avg. #{stats.average} milliseconds"
      end
    end
  end
end
