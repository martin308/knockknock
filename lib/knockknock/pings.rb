require "net/ping"

module Knockknock
  class Pings
    include Enumerable

    attr_accessor :ping_type, :host

    Stats = Struct.new(:host, :count, :average)

    def initialize(host:, ping_type: Net::Ping::HTTP)
      @host = host
      @ping_type = ping_type
    end

    # Generate the ping stats from the provided interval and times to ping
    def generate_stats(interval: 10, times: 6)
      results = with_interval(interval).take(times)
      count = results.length
      average = results
        .map(&:duration)
        .inject(&:+)
        .fdiv(count) * 1000
      Stats.new(host, results.count, average)
    end

    # Enumerate pings with an interval between each
    def with_interval(interval = 10, &block)
      Enumerator.new do |y|
        loop do
          y.yield first
          sleep interval
        end
      end
    end

    # Enumerable implementation, returns populated ping objects
    def each(&block)
      loop do
        yield begin
          ping
        end
      end
    end

    # Return a new ping object populated with the ping stats
    def ping
      ping_type.new(host).tap do |pinger|
        pinger.ping
      end
    end
  end
end
